/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.oxoop;

/**
 *
 * @author ACER
 */
public class Player {
    private char symbol;
    private int win;
    private int loss;
    private int draw;
    
    public Player(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public void win() {
        this.win++;
    }

    public void loss() {
        this.loss++;
    }

    public void draw() {
        this.draw++;
    }

    @Override
    public String toString() {
         return "Player "+symbol+" win: "+win+" loss: "+loss+" draw: "+draw;
    }
    
    public int getDraw() {
        return draw;
    }
    
}
