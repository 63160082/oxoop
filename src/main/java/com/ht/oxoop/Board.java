/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.oxoop;

/**
 *
 * @author ACER
 */
public class Board {
    
    private char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player O;
    private Player X;
    private int count;
    private int row , col;

    public boolean win = false;
    public boolean draw = false;
    

    public Board(Player O , Player X) {
        this.currentPlayer = X;
        this.O = O;
        this.X = X;
        this.count = 0;
    }
    
        public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getO() {
        return O;
    }

    public Player getX() {
        return X;
    }

    public int getCount() {
        return count;
    }
    
    public boolean setRowCol(int row , int col) {
        this.row = row;
        this.col = col;
        
        if(row > 3 || col > 3 || row < 1 || col <1) {
            return false;
        }
        if(table[row - 1][col - 1] !='-') {
            return false;
    }
        table[row - 1][col - 1] = currentPlayer.getSymbol();
        if (checkWin(row, col)) {
            updateStatWin();
            this.win = true;
            return true;
        }

        if(updateStatDraw()) {
            return true;
        }
        count++;
        switchPlayer();
        return true;
    }
    
    private boolean updateStatDraw() {
        if(checkDraw()) {
            O.draw();
            X.draw();
            this.draw = true;
            return true;
        }
        return false;
    }
     
    private void updateStatWin() {
        if(this.currentPlayer == X) {
            X.win();
            O.loss();
        } else {
            X.loss();
            O.win();
        }
    }

        public boolean isWin() {
            return win;
        }
        
        public boolean isDraw() {
            return draw;
        }
    
        public void switchPlayer() {
        if (currentPlayer == X) {
            currentPlayer = O;
        } else {
            currentPlayer = X;
        }
    }
        
    public boolean checkWin(int row , int col) {
        if(checkVertical()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkDiagonal()) {
            return true;
        }
        return false;
    }     

    public boolean checkVertical() {
        for (int row = 0; row < table.length; row++) {
            if (table[row][col - 1] != currentPlayer.getSymbol()) {
                return false;
            }
        }
            return true;   
        }
    
    public boolean checkHorizontal() {
        for (int col = 0; col < table.length; col++) {
            if (table[row - 1][col] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }
    
    public boolean checkDiagonal() {
        if (checkLeftDiagonal()) {
            return true;
        } else if (checkRightDiagonal()) { 
            return true;
        }
        return false;        
    }

    public  boolean checkLeftDiagonal() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }
    
    public boolean checkRightDiagonal() {  
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }


    
    

}
